## src/styles

Contains CSS styles common to all parts of the app. Specific rules should be placed in component/form/view folders