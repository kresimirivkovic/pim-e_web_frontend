const path = require('path');

module.exports = {
    /**
     * Stubs a file, transforming it into its filename with path relative to jest <rootDir> (and unix-style path separators)
     * Used to verify correct file was loaded in tests without actually loading the file
     */
    process: (src, filename, config, options) => {

        const rootDir = path.resolve('<rootDir>').replace(/<rootDir>$/, ''),
            relative  = filename.replace(rootDir, '').replace(/\\/g, '/'),
            escaped   = JSON.stringify(relative).replace(/^"/, '').replace(/"$/, '');

        return `module.exports = '${escaped}';`
    }
};