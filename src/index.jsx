import React from 'react'
import ReactDOM from 'react-dom'
import {AppContainer} from 'react-hot-loader'
import Routes from './routes'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

import 'styles/index.scss';

ReactDOM.render(
    <AppContainer>
        <MuiThemeProvider>
            <Routes />
        </MuiThemeProvider>
    </AppContainer>,
    document.getElementById('app')
);
