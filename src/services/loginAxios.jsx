import axios from 'axios';

const loginInstance = axios.create({
    baseURL: 'http://localhost:8080/web/'
});

loginInstance.interceptors.response.use((response) => {
    if(response.data.token){
       localStorage.setItem("token", response.data.token );
    }
    return response;
}, function (error) {
    return Promise.reject(error);
});

export default loginInstance;