class ProductService {

    static get(key) {
        const data = require(`../__tests__/samples/product-${key}.json`); // temporary mock
        return ProductService.extract(data);
    }

    static extract(data) {

        const getTypeValueOptions = (element) => {

            let type, value, elemType, val, options;

            switch (element.inputType) {

                case 'INTEGER':
                case 'DECIMAL':
                case 'INPUT':
                switch (element.elementType) {
                    case 'String' :
                        elemType= 'text';
                        val = element.value
                        break;
                    case 'Number':
                    case 'Integer':
                        elemType= 'number';
                        val = element.value;
                        break;
                    case 'Property':
                        val = element.value.value;
                        break;
                    default:
                        elemType= 'text';
                }
                return { type: 'text', value: val, elementType: elemType }

                case 'TEXT':
                case 'EXTENDEDTEXT': return { type: 'textarea', value: element.value }
                case 'PRICE': return { type: 'price', value: element.value }
                case '': return { type: 'price', value: element.value }


                case 'SINGLESELECT':     return { type: 'dropdown', value: element.value.value+'' , options: element.value.options || [] }
                case 'MULTISELECT':      return { type: 'multiselect', value: element.value.value , options: element.value.options || [] }
                case 'MULTIVALUESTRING': return { type: 'multitext', value: element.value.value }

                case 'BOOLEAN':
                case 'BOOLEANOPTIONAL': return { type: 'toggle', value: element.value, elementType: element.elementType  }

                case 'DATE':    return { type: 'datepicker', value: element.value }
                case 'PRICEDATEINTERVAL':    return { type: 'pricedateinterval', value: element.value }
                case 'BLANK':   return { type: 'hr' }

                case 'CustomPropertyGroupId': return { type: 'dropdown', value: element.value.selectedSn+'' , options: element.value.options || [] }

                case 'CustomTracklist':
                case 'QUOTESRATINGSSINGLEVALUESTRING': return { type: 'table', value: element.value}

                // TODO: this is a special bullshit kind of singleselect dropdown that needs to be preprocessed
                case 'CUSTOMSINGLESELECT': return { type: 'text', value: 'CUSTOMSINGLESELECT placeholder' }

                // TODO: this bullshit is so special it can't even be called bullshit, maybe dogcrap?
                case 'CUSTOMCALCULATEDPRICES': return { type: 'text', value: 'CustomCalculatedPrices placeholder' }

                // TODO: this is a special case of table, the values need to be preprocessed first
                case 'SUPPLIERATTRIBUTE': return { type: 'table', value: element.value }

                // TODO: this is also a special case of table, the values need to be preprocessed in a different way first
                case 'SINGLEVALUESTRING':
                    switch(element.elementType){
                        case 'Prognosis' :
                            elemType= 'number';
                            val = element.value
                            break;
                        default:
                            elemType= 'text';
                    }

                    return { type: 'prognosis', value: element.value }

                default:
                    console.error(element);
                    throw `Unknown field type "${element.inputType}"`
            }
        }

        const formSections = data.map(section => ({
            label: section.label,
            fields: section.elements.map(element => ({
                key: `${section.label}--${element.name}`,
                label: element.label,
                readOnly: !element.changeable,
                ...getTypeValueOptions(element),
            }))
        }));

        const formData = {};
        formSections.map(section => section.fields.map(field => formData[field.key] = field.value));

        return { formData, formSections };
    }
}

export default ProductService;