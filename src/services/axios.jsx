import axios from 'axios';

const Instance = axios.create({ baseURL: 'http://localhost:8080/web/'});

Instance.interceptors.request.use((config)=>{
    // localStorage.setItem('token', '');
    // const token = localStorage.getItem("token");
    const token = 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJlY2l1c2VyIiwiZXhwIjoxNTE2NjIzNTQwLCJpYXQiOjE1MTYwMTg3NDB9._k7hoTEzS2vAEatUdf3s7FW3nQfsGm_2bo7Gjx6h2Oqt6Fj6vAVN1uVUq9PbASKDGHti19s7aK1NMwUtjXxc6g';
    config.headers.Authorization = `Bearer ${token}`;
    return config;
})
Instance.interceptors.response.use((config)=> {
    const token = localStorage.getItem("token");
    if(token === null) {
        return config.status = 401
    }
    return config;
})

export default Instance;