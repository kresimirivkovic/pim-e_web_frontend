import React      from 'react'
import BaseView from '../BaseView'
import Navigation from '../../components/Navigation/Navigation'
import {HashRouter, Route} from 'react-router-dom'
import Instance from '../../services/axios'
import Paper        from 'material-ui/Paper'
import DropDownMenu from 'material-ui/DropDownMenu'
import MenuItem     from 'material-ui/MenuItem'
import RaisedButton from 'material-ui/RaisedButton';
import DynamicTab from '../DynamicTab'
import ProductService from '../../services/ProductService'
import SearchProduct from "../SearchProduct/search_Product";

import './style.scss'

class Products extends BaseView {

    constructor(props) {
        super(props);
        this.state = {
            catalogs: [],
            selectDropDownItem: '',
            subroutes: []
        }
        this.handleCatalogChange = this.handleCatalogChange.bind(this);
    }

    componentWillMount() {
        // do rest call...
        //
        const tabs = require('../../__tests__/samples/product-tabs.json');
        //

        this.setState({ subroutes: Object.keys(tabs).map(i => ({
            path: `/products/${tabs[i][0].replace(' ','_').toLowerCase()}`,
            key: i,
            name: tabs[i][0],
            title: tabs[i][1],
        }))});
    }

    componentDidMount(){
        Instance.get('/maintenance/catalogs')
            .then(response => {
                this.setState({catalogs :response.data})
                this.setState({selectDropDownItem : this.state.catalogs[0]})
            })
            .catch(function (error) {
                console.log(error);
            });

    }

    handleCatalogChange(){

    }

    renderSubroute(subroute, props) {
        return <DynamicTab route={subroute} service={ProductService} />
    }

    render() {

        return (
            <div className="view">
                <Paper className="full-width-block product-search" zDepth={1}>
                    <div className="flex-container">
                        <div className="flex-item">
                            <SearchProduct />
                        </div>
                        <div className="flex-item">
                            <DropDownMenu  value={this.state.selectDropDownItem.catalogSn} onChange={this.handleCatalogChange} style={{width: 300}}>
                                {this.state.catalogs.map(catalog => (
                                    <MenuItem value={catalog.catalogSn} primaryText={catalog.catalogId} key={catalog.catalogSn} />))}
                            </DropDownMenu>
                        </div>
                        <div className="flex-item">
                            <RaisedButton label="Search" primary={true} style={{width: 140}} />
                        </div>
                    </div>
                </Paper>
                <Paper className="full-width-block product-info" zDepth={1}>Product info</Paper>
                <div className="wrapper">
                    <HashRouter>
                        <div>
                        <Navigation routes={this.state.subroutes} nested={true} />
                        <Route
                            path={`${this.props.route.path}/404`}
                            render={() => <Paper className="contents" style={{marginLeft: 0}}>
                                <h3>Not found</h3>
                            </Paper>}
                            key={404}
                            />
                        {this.state.subroutes.map((subroute, i) => <Route
                            render={this.renderSubroute.bind(this, subroute)}
                            path={subroute.path}
                            key={i}
                            />)}
                        </div>
                    </HashRouter>
                </div>
            </div>
        );
    }
}

export default Products;
