import React from 'react'
import BaseView from '../BaseView'
import Form from '../../forms/Form'

import Paper from 'material-ui/Paper'
import RaisedButton from 'material-ui/RaisedButton'

class ContentUpload extends BaseView {

    saveMapping() {

    }

    importFile() {

    }

    componentWillMount() {

        this.formSections = [
            {
                label: 'Import',
                fields: [
                    { label: 'Import process' , type: 'dropdown' , options: { 0: 'lorem1' } },
                    { label: 'Catalogue'      , type: 'dropdown' , options: { 0: 'lorem1' } },
                    { label: 'File'           , type: 'filepicker' },
                ],
            },
            {
                label: 'Mapping',
                fields: [
                    { label: '/* mappingRow */' , type: 'mappingRow' },
                ]
            },
        ];

        this.formButtons = [
            { label: 'Save mapping', onClick: () => this.saveMapping() },
            { label: 'Import file' , onClick: () => this.importFile()  },
        ];
    }
}

export default ContentUpload;
