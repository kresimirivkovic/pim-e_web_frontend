import React from 'react'
import BaseView from '../BaseView'
import Form from '../../forms/Form'

import Paper from 'material-ui/Paper'

class ContentResult extends BaseView {

    componentWillMount() {
        this.formSections = [];
        this.formButtons = [];
    }
}

export default ContentResult;