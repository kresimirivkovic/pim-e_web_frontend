import React, { Component } from "react";
import { withRouter } from "react-router-dom";


class LogOut extends Component {
    constructor(props){
        super(props);
    }

    componentWillMount () {
        const { history } = this.props;
        history.push('/login')
    }
    render() {
        return null;
    }
};
export default withRouter(LogOut);