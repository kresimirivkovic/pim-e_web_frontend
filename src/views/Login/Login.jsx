import React, { Component } from "react";
import TextField from 'material-ui/TextField/TextField'
import AppBar from 'material-ui/AppBar'
import FlatButton from 'material-ui/FlatButton/FlatButton'
import loginAxios from '../../services/loginAxios'
import './login.css'

class Login extends Component {
    constructor(props){

        super(props);
        this.state={
            username:'',
            password:''
        }
        this.handleLoginClick = this.handleLoginClick.bind(this);
        this.validateForm = this.validateForm.bind(this);
        this.setUserName = this.setUserName.bind(this);
        this.setPassword = this.setPassword.bind(this);

    }
    validateForm() {
        return this.state.username.length > 0 && this.state.password.length > 0;
    }
    setUserName(event){
        this.setState({username: event.target.value})
    }
    setPassword(event){
        this.setState({password: event.target.value})
    }
    handleLoginClick(event){
        const { history } = this.props;
        const payload={
            "username":this.state.username,
            "password":this.state.password
        }
        loginAxios.post('/auth', payload)
            .then(function (response) {
                if(response.status == 200){
                    history.push('/home'); // TODO: this should be the previous path, not /home
                }
                else if(response.status == 204){
                    alert("username password do not match")
                }
                else{
                    alert("Username does not exist");
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }
    validateInput(username, password){
        return {
            username: username.length === 0,  password: password.length === 0,
        };
    }

    render() {
        const errors = this.validateInput(this.state.username, this.state.password);
        return (
            <div className="center">
                <form onSubmit={this.handleLoginClick}>
                        <div>
                            <AppBar
                                title="Login"
                            />
                            <TextField
                                className={errors.username ? "error": ""}
                                label="User name"
                                floatingLabelText="Username"
                                onChange = {this.setUserName}
                                margin="normal"
                            />
                            <br/>
                            <TextField
                                className={errors.password ? "error": ""}
                                label="Password"
                                type="password"
                                floatingLabelText="Password"
                                onChange = {this.setPassword}
                                margin="normal"
                            />
                            <br/>
                            <br/>
                            <FlatButton color="primary"
                                    raised="true"
                                    className="center"
                                    label="Login"
                                    disabled={!this.validateForm()}
                                    onClick={this.handleLoginClick}>
                            </FlatButton>
                        </div>

                </form>
            </div>
        );
    }
};
export default Login;