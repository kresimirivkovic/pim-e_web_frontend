import React from 'react'
import BaseView from '../BaseView'
import Form from '../../forms/Form'

import RaisedButton from 'material-ui/RaisedButton'
import Paper from 'material-ui/Paper'

class SearchConfigurations extends BaseView {

    delete() {

    }

    save() {

    }

    componentWillMount() {

        this.formSections = [
            {
                label: '',
                fields: [
                    { label: 'Select configuration' , type: 'dropdown' , options: { 0: 'lorem1' } },
                    { label: 'New' , type: 'button' , inline: true },
                ],
            }
        ];

        this.formButtons = [
            { label: 'Delete', onClick: () => this.delete() , secondary: true   },
            { label: 'Save'  , onClick: () => this.save()   , primary: true     },
        ];
    }
}

export default SearchConfigurations;
