import React from 'react'
import BaseView from '../BaseView'

import Navigation from '../../components/Navigation/Navigation'

class Search extends BaseView {

    render() {
        return (
            <div className="view">
                <div className="wrapper">
                    <Navigation routes={this.props.subroutes} nested={true} />
                </div>
            </div>
        );
    }
}

export default Search;
