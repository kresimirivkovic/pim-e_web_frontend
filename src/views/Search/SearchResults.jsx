import React from 'react'
import BaseView from '../BaseView'
import Form from '../../forms/Form'

import RaisedButton from 'material-ui/RaisedButton'
import Paper from 'material-ui/Paper'

class SearchResults extends BaseView {

    refresh() {

    }

    componentWillMount() {

        this.formSections = [
            {
                label: '',
                fields: [
                    { label: '/* dataTable */' , type: 'dataTable' },
                ],
            }
        ];

        this.formButtons = [
            { label: 'Refresh', onClick: () => this.refresh() },
        ];
    }
}

export default SearchResults;
