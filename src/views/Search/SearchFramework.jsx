import React from 'react'
import BaseView from '../BaseView'
import Form from '../../forms/Form'

import RaisedButton from 'material-ui/RaisedButton'
import Paper from 'material-ui/Paper'

class SearchFramework extends BaseView {

    componentWillMount() {

        this.formSections = [
            {
                label: '',
                fields: [
                    { label: 'Search configuration' , type: 'dropdown'    , options: { 0: 'lorem1' } },
                    { label: 'Search layout'        , type: 'dropdown'    , options: { 0: 'lorem1' } },
                    { label: ''                     , type: 'placeholder' , readOnly: true           },
                ],
            }
        ];

        this.formButtons = [
            { label: 'Search', onClick: () => this.form.submit() , primary: true   },
        ];
    }
}

export default SearchFramework;
