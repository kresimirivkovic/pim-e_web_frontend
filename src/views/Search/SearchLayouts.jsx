import React from 'react'
import BaseView from '../BaseView'
import Form from '../../forms/Form'

import RaisedButton from 'material-ui/RaisedButton'
import Paper from 'material-ui/Paper'

class SearchLayouts extends BaseView {

    delete() {

    }

    save() {

    }

    componentWillMount() {

        this.formSections = [
            {
                label: '',
                fields: [
                    { label: 'Select layout' , type: 'dropdown' , options: { 0: 'lorem1' } },
                    { label: 'New' , type: 'button' , inline: true },
                ],
            }
        ];

        this.formButtons = [
            { label: 'Delete', onClick: () => this.delete() , secondary: true   },
            { label: 'Save'  , onClick: () => this.save()   , primary: true     },
        ];
    }
}

export default SearchLayouts;
