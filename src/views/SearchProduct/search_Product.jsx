import React, {Component} from 'react'
import FlatButton from 'material-ui/FlatButton/FlatButton'
import TextField from 'material-ui/TextField/TextField'
import Instance from '../../services/axios'
import Input from '../../components/Input/Input'
import _ from 'lodash'


/**
 *
 * TODO: this is a component and not a view, should be integrated into the search inside Product.jsx
 *
 */

class SearchProduct extends Component {

    constructor(props){
        super(props);
        this.state = {
            searchProductId: '',
            comments: [],
            searchedproduct: '',

        }
        this.setSearchProductId = this.setSearchProductId.bind(this);
        this.searchProduct = this.searchProduct.bind(this);
    }
    setSearchProductId(event){
        this.setState({searchProductId: event.target.value});
    }

    searchProduct(event){
        event.preventDefault();
        const { history } = this.props;
        const url = 'product/searchProduct/' + this.state.searchProductId.trim();

        Instance.get(url)
            .then(response => {
                this.setState({searchedproduct: response.data})
                console.log('product ', sthis.state.searchedproduct)
            })
            .catch(function (error) {
                console.log(error);
            });
        Instance.get('maintenance/loadComments/'+ this.state.searchProductId)
            .then(response =>{
                this.setState({comments: response.data})
                console.log('the value of comments is ', this.state.comments );
            })
            .catch(function (error) {
                console.log(error);
            });
    }
    render() {
        return (
            <div>
                <div>
                <TextField
                    label="Search product"
                    floatingLabelText="Product ID"
                    onChange = {this.setSearchProductId}
                    margin="normal"
                    value={this.state.searchProductId}
                />
                    <FlatButton color="primary" raised="true" className="center"  label="Search Product" onClick={this.searchProduct}></FlatButton>
                </div>

            </div>
        );
    }
}

export default SearchProduct;
