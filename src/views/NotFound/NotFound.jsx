import React from 'react'
import BaseView from '../BaseView'

import Paper from 'material-ui/Paper'

class NotFound extends BaseView {

    render() {
        return (
            <Paper className="contents">
                <h2>Page not found</h2>
            </Paper>
        );
    }
}

export default NotFound;
