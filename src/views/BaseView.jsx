import React from 'react'
import Paper from 'material-ui/Paper'
import Form from '../forms/Form'

import './style.scss';

class BaseView extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Paper className="contents">
                <Form
                    ref={(form) => this.form = form}
                    data={{}}
                    sections={this.formSections}
                    buttons={this.formButtons} />
            </Paper>
        );
    }
}

export default BaseView;
