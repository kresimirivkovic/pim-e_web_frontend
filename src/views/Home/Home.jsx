import React from 'react'
import BaseView from '../BaseView'
import Button from '../../components/Button/Button'

import './style.scss'

class Home extends BaseView {

    render() {
        return (
            <div className="view">
                <h2>Home</h2>
                <div className="contents">
                    Welcome to PIM-e web
                </div>
            </div>
        );
    }
}

export default Home;
