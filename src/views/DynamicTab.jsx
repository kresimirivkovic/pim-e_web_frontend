import React from 'react'
import Form from '../forms/Form'
import Paper from 'material-ui/Paper'
import {withRouter} from 'react-router-dom'

class DynamicTab extends React.Component {

    constructor(props) {
        super(props);
        this.name = this.props.route.name;
        this.state = {
            formSections: [],
            formButtons: [
                { label: 'Submit', onClick: () => this.form.submit() , primary: true   },
                { label: 'Clear' , onClick: () => this.form.clear()  , secondary: true },
            ],
            formData: {}
        };
    }

    componentDidMount() {
        try {
            const {formSections, formData} = this.props.service.get(this.props.route.key);

            this.setState({
                formSections: formSections,
                formData: formData
            });

        } catch (error) {
            this.props.history.push({ pathname: '404' });
            console.error(error);
        }
    }

    render() {
        return <Paper className="contents" style={{marginLeft: 0}}>
                <Form
                    ref={(form) => this.form = form}
                    data={this.state.formData}
                    sections={this.state.formSections}
                    buttons={this.state.formButtons} />
            </Paper>;
    }
}

export default withRouter(DynamicTab);
