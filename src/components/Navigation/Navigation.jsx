import React from 'react'
import {matchPath} from 'react-router'
import {withRouter} from 'react-router-dom'

import {Tabs, Tab} from 'material-ui/Tabs'
import Slider from 'material-ui/Slider'
import {colors} from 'material-ui/styles'

import './style.scss'

class Navigation extends React.Component {

    constructor(props) {
        super(props);

        const flatRoutes = [];
        this.props.routes.map(route => {
            flatRoutes.push(route);
            (route.subroutes || []).map(subroute => flatRoutes.push(subroute));
        });

        const activePaths = flatRoutes.filter(route => matchPath(this.props.location.pathname, route)),
            index = !this.props.nested ? 0 : activePaths.length - 1; // the parent navigation uses the first matched route, child navs use last match

        if (activePaths.length && activePaths[index].path) {
            this.state = { tabSelected: activePaths[index].path };

        } else if (this.props.nested) {
            this.state = { tabSelected: this.props.routes[0].path };
            this.props.history.push(this.props.routes[0].path);

        } else {
            this.props.history.push({ pathname: '/404' });
        }

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange (path) {
        this.props.history.push({ pathname: path });
        this.setState({ tabSelected: path });
    }

    shouldComponentUpdate(nextProps, nextState) {
        return this.state.tabSelected != nextState.tabSelected;
    }

    render() {
        let overrideStyle = {};
        if (this.props.nested) {
            overrideStyle = { style: {
                color: colors.grey500,
                backgroundColor: colors.grey200,
                whiteSpace: 'normal'
            }}
        }

        return (
            <Tabs className="navigation" value={this.state.tabSelected} onChange={this.handleChange}>
                {this.props.routes
                    .filter(route => !route.hidden)
                    .map((route, i) => <Tab {...overrideStyle} label={route.title} value={route.path} key={i} />)}
            </Tabs>
        );
    }
}

export default withRouter(Navigation);
