import React, {Component} from 'react';
import MuiDatePicker from 'material-ui/DatePicker'
import './PriceDateInterVal.css'
const image = require('../../assets/images/ic_delete_black_24dp_1x.png')


class Period extends Component {
    constructor(props) {
        super(props);

        this.state = {
            startDate: this.props.value.startDate === null ? new Date(): new Date(this.props.value.startDate),
            endDate: typeof this.props.value.endDate === 'number'? new Date(this.props.value.endDate) : new Date().getTime(),
            className: "after-box",
        };
    }
    checkDates=(event, value) =>{
        console.log('the event and value are  ', event , ' ', value)
    }
    removePeriod=()=> {
        //here we should send http request to delete this component in back-end
        this.setState({
            className: "hidden"
        });
    }
    handleDateChange=(event, value) =>{
        console.log('event and value are ', event.target.value , ' value ', value);
    }
    render(){
        return (
            <div className={this.state.className}>
                <MuiDatePicker
                    {...this.props}
                    autoOk={false}
                    onChange={(event, value) => this.handleDateChange}
                    value={this.state.startDate}
                    className="floating-box"
                    onBlure={(event, value) => this.checkDates}
                />
                <MuiDatePicker
                    {...this.props}
                    autoOk={false}
                    onChange={(event, value) => this.handleDateChange}
                    value={this.state.endDate}
                    className="floating-box"
                    onBlure={(event, value) => this.checkDates}
                />
                <img src={image} onClick={this.removePeriod}/>
            </div>
        );

    }
}

export default Period;