import React, {Component} from 'react';
import MuiDatePicker from 'material-ui/DatePicker'
import MuiTextField from "material-ui/TextField/index"
import NumberFormat from 'react-number-format'
import './PriceDateInterVal.css'
const image = require('../../assets/images/ic_delete_black_24dp_1x.png')


class PriceDateInterval extends Component {
    constructor(props) {
        super(props);

        this.state = {
            amount: this.props.value.amount,
            startDate: this.props.value.startDate === null ? new Date(): new Date(this.props.value.startDate),
            endDate: typeof this.props.value.endDate === 'number'? new Date(this.props.value.endDate) : new Date().getTime(),
            className: "after-box",
        };
    }
    numFormatToCurrency =(amount)=> {
        if (+amount) {
            return (+amount).toLocaleString("nl-NL", {minimumFractionDigits: 2, maximumFractionDigits: 2} );
        } else {
            return amount;
        }
    }
    removeEuroSignAndSetState = (event) =>{
        const amountWithoutEuroSign = event.target.value.substring(1);
        if(amountWithoutEuroSign != '' || amountWithoutEuroSign != null ) {
           const amount = amountWithoutEuroSign.replace(/[,]/, ".")
             this.setState = ({
                 amount: amount,
                 className: "floating-box"
             });
        } else {
             this.setState =({amount: amountWithoutEuroSign, className: "floating-box"});
        }
    }
    removePriceDate=()=> {
        //here we should send http request to delete this component
        this.setState({
            className: "hidden"
        });
    }
    handleStartDateChange=(event, date)=>{
        this.setState({
            startDate: date
        });
    }
    handleEndDateChange=(event, date)=>{
        this.setState({
            endDate: date
        });
    }
    render(){
        return (
            <div className={this.state.className}>
                    <NumberFormat
                        onChange={(event) => this.removeEuroSignAndSetState(event)}
                        value={this.numFormatToCurrency(this.state.amount)}
                        customInput={MuiTextField}
                        decimalSeparator=","
                        prefix={'€'}
                        className="floating-box"
                    />
                    <MuiDatePicker
                        {...this.props}
                        autoOk="false"
                        onChange={(event, date) => this.handleStartDateChange(event,date)}
                        value={this.state.startDate}
                        className="floating-box"
                    />
                   <MuiDatePicker
                        {...this.props}
                        autoOk="false"
                        onChange={(event, date) => this.handleEndDateChange(event,date)}
                        value={this.state.endDate}
                        className="floating-box"
                  />
                   <img src={image} onClick={this.removePriceDate}/>
            </div>
        );

    }

}
export default PriceDateInterval