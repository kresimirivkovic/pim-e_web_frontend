import React, {Component} from 'react'
import MuiTextField from 'material-ui/TextField'
import './PriceDateInterVal.css'

class Prognosis extends Component {
    constructor(props) {
        super(props);
        this.state = {
            valueBE: this.props.value["BE"].value ,
            valueNL: this.props.value["NL"].value,
            valueTotal: this.props.value["Total"].value
        };
    }
    calculateTotal=() =>{
      return (+this.state.valueBE) + (+this.state.valueNL);
    }
    handleInput=(event)=>{
        console.log('event.target', event.target.id)
            event.target.id=== "NL"? this.setState({valueNL: event.target.value}): this.setState({valueBE: event.target.value})

    }

    render(){
        return (
           <div className="box">
                    <MuiTextField
                        label={this.props.label}
                        id="NL"
                        onChange={(event) => this.handleInput(event)}
                        value={this.state.valueNL}
                        type="number"
                        className="inside-box"
                    />
                    <MuiTextField
                        id="BE"
                        onChange={(event) => this.handleInput(event)}
                        value={this.state.valueBE}
                        type="number"
                        className="inside-box"
                    />

                <div >
                    <MuiTextField
                        id="total"
                        value={this.calculateTotal()}
                        type="number"
                        disabled={true}
                        className="inside-box"
                    />
                </div>
           </div>

        );
    }
}

export default Prognosis;