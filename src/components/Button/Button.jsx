import React from 'react'
import MaterialUiButton from 'material-ui/RaisedButton'

import './style.scss'

class Button extends React.Component {

    constructor(props) {
        super(props);
    }

    shouldComponentUpdate(prevProps, prevState) {
        return false;
    }

    render() {
        return (
            <span className="custom-button">
                <MaterialUiButton {...this.props} />
            </span>
        );
    }
}

export default Button;
