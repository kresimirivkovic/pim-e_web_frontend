import React, {Component} from 'react';
import MuiTextField from 'material-ui/TextField'
import NumberFormat from 'react-number-format';

class PriceField extends Component {
    constructor(props) {
        super(props);
        this.state = {
            amount: this.props.value.amount
        };
    }
    numFormatToCurrency =(amount)=> {
        console.log('amount is ', amount)
        if (+amount) {
            return (+amount).toLocaleString("nl-NL", {minimumFractionDigits: 2, maximumFractionDigits: 2} );
        } else {
            return amount;
        }
    }
    removeEuroSignAndSetState = (event) =>{
        const amountWithoutEuroSign = event.target.value.substring(1);
        const amount = amountWithoutEuroSign.replace(/[,]/, ".");
        this.setState({ amount: amount});
    }
    render() {
        return (
            <NumberFormat
                onChange={(event) => this.removeEuroSignAndSetState(event)}
                value={this.numFormatToCurrency(this.state.amount)}
                customInput={MuiTextField}
                decimalSeparator=","
                prefix={'€'}
            />
        )
    }
}


export default PriceField