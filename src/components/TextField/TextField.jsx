import React from 'react'
import MuiTextField from 'material-ui/TextField'

class TextField extends React.Component {

    constructor(props) {
        super(props);

        if (this.props.multivalue) {
            this.state = { value: (this.props.value || []).join('\n')+'\n', values: this.props.value };
            // TODO: still need to handle this special case in onChange

        } else {
            this.state = { value: this.props.value };
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.state.value != nextState.value || this.state.values != nextState.values) {
            return true;
        }
        return false;
    }

    render() {
        return <MuiTextField
            {...this.props}
            onChange={(event, value) => this.setState({ value: value }) }
            value={this.state.value}
            type={this.props.type}
            />
    }

}

export default TextField
