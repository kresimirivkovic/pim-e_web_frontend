import React,{Component} from 'react'
import _ from "lodash";
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';


class TableField extends Component { // this implements a widget containing a row of input fields

	constructor(props) {
		super(props);
            this.state = { value: this.props.value};
	}

    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }

	render () {
	    // get colunm's name
	    const nameArray = Object.getOwnPropertyNames(this.state.value[0]);
		return (

            <Table>
                <TableHeader>
                    <TableRow>
                        {nameArray.map(key => (
                             <TableHeaderColumn key={key}>{key}</TableHeaderColumn>
                        ))}
                    </TableRow>
                </TableHeader>

                <TableBody displayRowCheckbox={false}>
                    {this.state.value.map((element,i) => (
                        <TableRow key={i}>
                            {Object.getOwnPropertyNames(element).map(key =>(
                                <TableRowColumn key={key}>{element[key]}</TableRowColumn>
                            ))}
                        </TableRow>
                    ))};
                </TableBody>
            </Table>
        );
	}
}

export default TableField;
