import React from 'react';
import TextField from 'material-ui/TextField/TextField'
import DatePicker from 'material-ui/DatePicker/DatePicker'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem';
import classes from './Input.css';
import _ from 'lodash'

const input = ( props ) => {
    const getValue = ((val) =>{
        if(val === Object(val)){
            return val.valueType
        }
        return val;
    });
    const convertToDate = ((val) => {
        return new Date(val);
    });
    let inputElement = null;
    const inputClasses = [classes.InputElement];

    // if (props.invalid && props.shouldValidate && props.touched) {
    //     inputClasses.push(classes.Invalid);
    // }
    const menuItems = (options, values) => {
        return _.keys(options).map((key) => (
            <MenuItem
                key = {key}
                insetChildren = {true}
                checked={values && values.indexOf(key) > -1}
                value = {key}
                primaryText = {options[key]}
            />
        ));
    }

    switch ( props.inputType ) {
        case ('INPUT' ):
            inputElement =
                <TextField
                    id={props.id}
                    label={props.label}
                    className={inputClasses.join(' ')}
                    type="text"
                    key={props.id}
                    value={getValue(props.value)}
                    onChange={props.changed}
                    disabled={!props.visible}>
                </TextField>
            break;
        case ( 'DATE' ):
            inputElement =
                <DatePicker
                    label={props.label}
                    className={inputClasses.join(' ')}
                    key={props.id}
                    value={convertToDate(props.value)}
                    onChange={props.changed}
                    disabled={!props.visible}
                    style={{color: props.color}}
                />;
            break;
        case ( 'PERIOD' ):
            inputElement =
                <div>
                    <DatePicker
                        label={props.label}
                        className={inputClasses.join(' ')}
                        key={props.id}
                        value={convertToDate(props.value)}
                        onChange={props.changed}
                        disabled={!props.visible}
                        style={{color: props.color}}
                    />
                    <span></span>
                    <DatePicker
                        label={props.label}
                        className={inputClasses.join(' ')}
                        key={props.id}
                        value={convertToDate(props.value)}
                        onChange={props.changed}
                        disabled={!props.visible}
                        style={{color: props.color}}
                    />;
                </div>
            break;
        case ('TEXT'):
            inputElement =
                <TextField
                    label={props.label}
                    className={inputClasses.join(' ')}
                    key={props.id}
                    value={getValue(props.value)}
                    onChange={props.changed}
                    disabled={!props.visible}
                    multiLine="true"
                />

            break;
        case ('NUMBER'):
            inputElement =
                <NumberField
                    label={props.label}
                    type="number"
                    className={inputClasses.join(' ')}
                    key={props.id}
                    value={getValue(props.value)}
                    onChange={props.changed}
                    disabled={!props.visible}
                />
            break;
        case ('SINGLESELECT'):
            inputElement =
                <SelectField
                    label={props.label}
                    className={inputClasses.join(' ')}
                    key={props.id}
                    value={props.value}
                    disabled={!props.visible}
                    onChange={props.changed}>
                    {_.keys(props.value.options).map(key =>
                        <MenuItem
                            key={key}
                            value={key}
                            primaryText={props.value.options[key]}>
                        </MenuItem >
                    )}
                </SelectField>
            break;
        case ('MULTISELECT'):
            inputElement =
                <SelectField
                    label={props.label}
                    className={inputClasses.join(' ')}
                    multiple={true}
                    key={props.id}
                    value={props.value.value}
                    disabled={!props.visible}
                    onChange={props.changed}>
                    {menuItems(props.value.options, props.value.value)}
                </SelectField>
            break;
        case ('PRICE'):
            inputElement =
                <NumberField
                    label={props.label}
                    className={inputClasses.join(' ')}
                    key={props.id}
                    value={getValue(props.value)}
                    onChange={props.changed}
                    disabled={!props.visible}
                />
            break;
        case ('INTEGER'):
            inputElement =
                <NumberField
                    label={props.label}
                    type="number"
                    className={inputClasses.join(' ')}
                    key={props.id}
                    value={getValue(props.value)}
                    onChange={props.changed}
                    disabled={!props.visible}
                />
            break;
        case ('BOOLEAN'):
            inputElement =
                <SelectField
                    label={props.label}
                    className={inputClasses.join(' ')}
                    key={props.id}
                    value={props.value}
                    disabled={!props.visible}
                    onChange={props.changed}>
                      <MenuItem  key="yes"   value="yes"   primaryText="Yes"/>
                      <MenuItem  key="no"   value="no"   primaryText="No"/>
                </SelectField>
            break;
        case ('BOOLEANOPTIONAL'):
            inputElement =
                <SelectField
                    label={props.label}
                    className={inputClasses.join(' ')}
                    key={props.id}
                    value={props.value}
                    onChange={props.changed}>
                    <MenuItem  key="0"  value=""  primaryText=""/>
                    <MenuItem  key="1"  value="yes" primaryText="Yes"/>
                    <MenuItem  key="2"  value="no"  primaryText="No"/>
                </SelectField>
            break;

        default:
            inputElement = <input
                className={inputClasses.join(' ')}
                value={props.value}
                onChange={props.changed} />;
    }

    return (
        <div>
            <label className={classes.Label}>{props.label}</label>
            <div></div>
            {inputElement}
        </div>
    );

};

export default input;