import React from 'react'
import MuiDatePicker from 'material-ui/DatePicker'

class Datepicker extends React.Component {

    constructor(props) {
        super(props);

        const value = typeof this.props.value === 'number'
            ? new Date(this.props.value)
            : this.props.value === ''
                ? null
                : this.props.value;

        this.state = { value: value };
    }

    shouldComponentUpdate(prevProps, prevState) {
        return this.state.value != prevState.value;
    }

    render() {
        return <MuiDatePicker
            {...this.props}
            autoOk={true}
            value={this.state.value}
            />
    }

}

export default Datepicker
