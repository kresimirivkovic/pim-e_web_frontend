import React from 'react'
import MuiSelectField from 'material-ui/SelectField'

class SelectField extends React.Component {

    constructor(props) {
        super(props);
        // todo: fix for multivalue
        this.state = { value: this.props.value };
    }

    shouldComponentUpdate(nextProps, nextState) {
        return this.state.value != nextState.value;
    }

    render() {
        return <MuiSelectField
            {...this.props}
            onChange={(event, index, value) => {
                this.setState({ value: value })
            } }
            value={this.state.value}
            />
    }

}

export default SelectField
