import React from 'react'
import MuiToggle from 'material-ui/Toggle'

class Toggle extends React.Component {

    constructor(props) {
        super(props);
        this.state = { value: !!this.props.value };
    }

    shouldComponentUpdate(nextProps, nextState) {
        return this.state.value != nextState.value;
    }

    render() {
        return <MuiToggle
            {...this.props}
            onChange={(event, value) => this.setState({ value: value }) }
            toggled={this.state.value}
            />
    }

}

export default Toggle
