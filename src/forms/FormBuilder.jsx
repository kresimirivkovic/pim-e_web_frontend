import React from 'react'

import DatePicker   from '../components/DatePicker/DatePicker'
import TextField    from '../components/TextField/TextField'
import Toggle       from '../components/Toggle/Toggle'
import SelectField  from '../components/SelectField/SelectField'
import TableField        from '../components/Table/Table'
import RaisedButton from 'material-ui/RaisedButton'
import MenuItem     from 'material-ui/MenuItem'
import PriceField from "../components/TextField/PriceField";
import PriceDateInterval from "../components/CustomComponent/PriceDateInterval";
import Prognosis from "../components/CustomComponent/Prognosis";


class FormBuilder {

    static getField(field) {

        const properties = {};
        properties.disabled = field.readOnly || false;
        properties.style = field.style || {};

        let name, children;

        switch (field.type) {

            case 'text':
                name = TextField;
                properties.floatingLabelText = field.label;
                properties.fullWidth = true;
                properties.type = field.elementType;
                break;

            case 'textarea':
            case 'multitext':
                name = TextField;
                properties.floatingLabelText = field.label;
                properties.fullWidth = true;
                properties.multiLine = true;
                properties.rows = 4;
                properties.multivalue = field.type === 'multitext' ? 'true' : null; // fucking react bullshit, see: https://github.com/facebook/react/issues/9230
                properties.rowsMax = 4;
                break;

            case 'price':
                name = PriceField;
                properties.floatingLabelText = field.label;
                properties.fullWidth = true;
                break;

            case 'datepicker':
                name = DatePicker;
                properties.floatingLabelText = field.label;
                properties.fullWidth = true;
                break;

            case 'pricedateinterval':
                name = PriceDateInterval;
                properties.floatingLabelText = field.label;
                properties.fullWidth = true;
                break;

            case 'filepicker':
                name = TextField;
                properties.floatingLabelText = field.label;
                properties.fullWidth = true;
                break;

            case 'dropdown':
            case 'multiselect':

                if (field.options.constructor !== Object) {
                    console.error(field);
                    throw `Invalid multiselect options`
                }

                children = Object.keys(field.options)
                    .map(key => <MenuItem key={key} value={key} primaryText={field.options[key]} />)

                name = SelectField;
                properties.multiple = field.type === 'multiselect';
                properties.floatingLabelText = field.label;
                properties.fullWidth = true;
                break;

            case 'toggle':
            case 'boolean':
                name = Toggle;
                properties.label = field.label;
                properties.className = 'checkbox';
                break;

            case 'button':
                name = RaisedButton;
                properties.label = field.label;
                break;
            case 'prognosis':
                name = Prognosis;
                properties.label = field.label;
                properties.type = field.elementType;
                break;

            case 'table':
                name = TableField;
                properties.label = field.label;
                break;

            // custom components will be handled here
            case 'mediaRow':
            case 'priceRow':
            case 'mappingRow':
            case 'dataTable':
                name = TextField;
                properties.floatingLabelText = field.label;
                break;

            case 'placeholder': // leave this for now
                name = 'div';
                break;

            case 'hr':
                name = 'hr';
                break;

            default:
                throw `Unknown field type "${field.type}"`;
        }

        return { name, properties, children };
    }

    static getOverrideClasses(field) {
        if (field.inline) {
            return 'inline-field';
        }
    }

    static createActionButton(properties) {
        return <RaisedButton {...properties} />
    }
}

export default FormBuilder;
