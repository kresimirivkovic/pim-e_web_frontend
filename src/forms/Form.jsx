import React from 'react'
import FormBuilder from './FormBuilder'
import Subheader    from 'material-ui/Subheader'
import Snackbar from 'material-ui/Snackbar'

import './style.scss'

class Form extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            snackbar: {}
        };

        this.submit = this.submit.bind(this);
        this.send   = this.send.bind(this);
        this.clear  = this.clear.bind(this);
        this.undo   = this.undo.bind(this);

        this.fields = {};
    }

    submit() {
        console.log('Called submit')

        this.blockWindowClose();

        // do validation...
        // call service...
        // handle errors/success...

        const data = {};
        Object.keys(this.fields).map(i => {
            data[i] = this.fields[i].state.value;
        });

        this.send.bind(this, data);
        this.actionClick = this.undo.bind(this);
        this.requestClose = this.send.bind(this, data)
        this.setState({ snackbar: { open: true, message: 'Changes have been saved', action: 'undo' } });
    }

    send(data) {
        this.setState({ snackbar: { open: false }}, () => {
            console.log('Sending...', data);
            this.unblockWindowClose();
        });
    }

    blockWindowClose() {
        window._$$onbeforeunload = window.onbeforeunload;
        window.onbeforeunload = function(evt) {
            return true;
        }
    }

    unblockWindowClose() {
        window.onbeforeunload = window._$$onbeforeunload;
    }

    undo() {
        this.requestClose = () => {
            this.unblockWindowClose();
            this.setState({ snackbar: { open: false } })
        };
        this.setState({ snackbar: { open: true, message: 'Submission cancelled', action: '' } });
        this.clear();
        this.unblockWindowClose();
    }

    clear() {
        //this.setState((prevState) => ({ values: prevState.history }));
    }

    render() {
        return <form className="form" ref={(form) => this.formDOM = form}>
            {this.props.sections.map(
                (section, i) => [
                <Subheader className="subheader" key={`subheader_${i}`}>
                    {section.label}
                </Subheader>,
                <div className="section" key={i}>
                    {section.fields.map(
                        (field, j) => {
                            const {name, properties, children} = FormBuilder.getField(field);
                            return <div className={`field ${FormBuilder.getOverrideClasses(field)}`} key={j}>
                                {React.createElement(
                                    name,
                                    {
                                        ...properties,
                                        value: this.props.data[field.key] || '',
                                        ref: (ref) => this.fields[field.key] = ref
                                    },
                                    children)}
                            </div>
                        }
                    )}
                </div>
                ]
            )}
            <div className="button-block">
                {this.props.buttons.map((button, i) => FormBuilder.createActionButton({
                    style: {width: 150, height: 50},
                    ...button,
                    key: i
                }))}
            </div>
            <Snackbar
                open={this.state.snackbar.open || false}
                message={this.state.snackbar.message || ''}
                action={this.state.snackbar.action || ''}
                autoHideDuration={4000}
                onActionClick={this.actionClick}
                onRequestClose={this.requestClose}
            />
        </form>;
    }
}

export default Form;
