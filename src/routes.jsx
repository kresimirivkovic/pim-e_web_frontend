import React from 'react'
import {HashRouter, Route} from 'react-router-dom'
import Navigation from './components/Navigation/Navigation'

import Home     from './views/Home/Home'
import Login  from './views/Login/Login'
import Logout  from './views/Login/logout'
import NotFound from './views/NotFound/NotFound'

import Products             from './views/Products/Products'

import MissingContent  from './views/MissingContent/MissingContent'
import ContentUpload   from './views/MissingContent/ContentUpload'
import ContentResult   from './views/MissingContent/ContentResult'
import ContentLog      from './views/MissingContent/ContentLog'

import Search               from './views/Search/Search'
import SearchFramework      from './views/Search/SearchFramework'
import SearchConfigurations from './views/Search/SearchConfigurations'
import SearchLayouts        from './views/Search/SearchLayouts'
import SearchResults        from './views/Search/SearchResults'

const routes = [
    {
        path: '/',
        view: Home,
        title: 'Home',
        exact: true
    },
    {
        path: '/login',
        view: Login,
        title: 'Login',
        hidden: true
    },
    {
        path: '/logout',
        view: Logout,
        title: 'Log Out',
        hidden: true
    },
    {
        path: '/products',
        view: Products,
        title: 'Products'
    },
    {
        path: '/missing_content',
        view: MissingContent,
        title: 'Missing Content',
        subroutes: [
            { path: '/upload' , title: 'Upload' , view: ContentUpload },
            { path: '/result' , title: 'Result' , view: ContentResult },
            { path: '/log'    , title: 'Log'    , view: ContentLog    },
        ]
    },
    {
        path: '/search_framework',
        view: Search,
        title: 'Search Framework',
        subroutes: [
            { path: '/search'        , title: 'Search'        , view: SearchFramework      },
            { path: '/configurations', title: 'Configurations', view: SearchConfigurations },
            { path: '/layouts'       , title: 'Layouts'       , view: SearchLayouts        },
            { path: '/results'       , title: 'Results'       , view: SearchResults        },
        ]
    },
    {
        path: '/404',
        view: NotFound,
        title: 'Page not found',
        hidden: true
    }
];

class Routes extends React.Component {

    constructor(props) {
        super(props);
    }

    render () {
        return (
            <HashRouter>
                <div>
                    <Navigation routes={routes} />
                    {routes.map((route, i) => {
                        const extra = {}, current = [];

                        if (route.exact) {
                            extra.exact = true;
                        }

                        current.push(
                            <Route
                                {...extra}
                                path={route.path}
                                render={(props) => React.createElement(route.view, { route: route, subroutes: route.subroutes || [] })}
                                key={`_${i}`}
                                />
                        );

                        route.subroutes = (route.subroutes || []).map((subroute, j) => {
                            subroute.path = `${route.path}${subroute.path}`;
                            current.push(
                                <Route
                                    {...extra}
                                    path={subroute.path}
                                    render={(props) => React.createElement(subroute.view, { route: subroute })}
                                    key={`${i}_${j}`}
                                    />
                            );
                            return subroute;
                        });

                        return <div key={i}>{current}</div>
                    })}
                </div>
            </HashRouter>
        );
    }
}

export default Routes;
