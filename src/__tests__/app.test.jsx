import React from 'react'
import {configure, mount} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import Home from '../views/Home/Home'

configure({
    adapter: new Adapter()
});

describe('Home', () => {
    it('renders ok', () => {
        mount(
            <MuiThemeProvider>
                <Home />
            </MuiThemeProvider>
        );
    });
});
